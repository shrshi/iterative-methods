%% Solving linear systems with Chebyshev accelerated incomplete Cholesky factorization
%% Sparse matrix : bcsstk01
% 48X48 SPD 400 NNZs K(A) ~ 10^5

figure;
[A, b] = load_mat('bcsstk01.mat');

subplot(1,2,1)
spy(A)
title('Sparsity pattern of A');

L = ichol(A);
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);

subplot(1,2,2)
semilogy(resvec)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
title('Convergence of Chebyshev solver');

%% Sparse matrix : bcsstk02
% 66X66 SPD 4356 NNZs K(A) ~ 10^3

figure;
[A, b] = load_mat('bcsstk02.mat');

subplot(1,2,1)
spy(A)
title('Sparsity pattern of A');

L = ichol(A);
[alpha, beta] = eigbounds(A, L);

resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);

subplot(1,2,2)
semilogy(resvec)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
title('Convergence of Chebyshev solver');

%%
% 
%  For dense matrices, IC is very close to complete Cholesky factorization,
%  as shown below.

% Densify A
A_dense = full(A);
R = chol(A_dense);
% The normed difference between IC and Cholesky
disp(norm(R'*R - full(L*L')));
%% Sparse matrix : 494_bus
% 494X494 SPD 1666 NNZs K(A) ~ 10^6

figure;
[A, b] = load_mat('494_bus.mat');

subplot(1,2,1)
spy(A)
title('Sparsity pattern of A');

L = ichol(A);
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);

subplot(1,2,2)
semilogy(resvec)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
title('Convergence of Chebyshev solver');

%% Sparse matrix : 662_bus
% 662X662 SPD 2474 NNZs K(A) ~ 10^5

figure;
[A, b] = load_mat('662_bus.mat');

subplot(1,2,1)
spy(A)
title('Sparsity pattern of A');

L = ichol(A);
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);

subplot(1,2,2)
semilogy(resvec)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
title('Convergence of Chebyshev solver');

%% Sparse matrix : bcsstk15
% 3948X3948 SPD 117816 NNZs K(A) ~ 10^9

figure;
[A, b] = load_mat('bcsstk15.mat');

subplot(1,2,1)
spy(A)
title('Sparsity pattern of A');

% since this matrix has a non positive pivot, we use a diagonal shift to
% make M = A + alpha*(diag(diag(A)) diagonally dominant
alph = max(sum(abs(A),2)./diag(A))-2;
L = ichol(A, struct('diagcomp', alph));
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);

subplot(1,2,2)
semilogy(resvec)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
title('Convergence of Chebyshev solver');

%% Varying dropping options for |ichol| for 494_bus matrix

[A, b] = load_mat('494_bus.mat');

ntols = 10;
droptol = logspace(-8,0,ntols);
best_convergence = inf;
best_sparsity = 500;
numiters = zeros(ntols, 1);

for k=1:ntols
    L = ichol(A, struct('type', 'ict', 'droptol', droptol(k)));
    [alpha, beta] = eigbounds(A, L);
    resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);
    numiters(k) = length(resvec);
    if(length(resvec) < best_convergence)
        best_convergence = length(resvec);
        best_droptol = droptol(k);
    end
    if(abs(nnz(L)*2 - nnz(A))<=best_sparsity)
        best_droptol_sparsity = droptol(k);
        best_sparsity = abs(nnz(L)*2 - nnz(A));
    end
end

figure;
loglog(droptol, numiters)
ylabel("Log numiters until convergence");
xlabel("Log of threshold tolerance");
title("Threshold based IF with residual norm tol = 1e-5")

L = ichol(A, struct('type', 'ict', 'droptol', best_droptol));
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);
figure;
semilogy(resvec)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
title('Convergence of Chebyshev solver for threshold tol = 1e-8');

L = ichol(A, struct('type', 'ict', 'droptol', best_droptol_sparsity));
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);
figure;
semilogy(resvec)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
title('Convergence of solver for 2*nnz(L) ~ nnz(A)');
%%
% 
%  Modified IC encounters nonpositive pivot error even after diagonal
%  shifting, and is hence not analysed.
% 

%% 
% 
%  
%  
%  From the above plots, it is clear that as the drop tolerance decreases,
%  the convergence rate of Chebyshev's algorithm increases. This is as
%  expected since the incomplete cholesky approximation approaches complete
%  cholesky factorization. The number of nnzs in L with zero droptol for 
%  this matrix is 1080. L, for drop tol = 1e-8, has 6561 non zeros while
%  A itself has only 1666 non zeros. If we were to find the drop tolerance 
%  for which twice the number of non zeros
%  in L is approximately equal to number of non zeros in A, then the best 
%  drop tol is 0.1292, for which abs(2*nnz(L) - nnz(A)) = 108. Though 
%  convergence has increased from 2 iterations, it is still lower (461
%  iterations) than when the drop tol is 0 (603 iterations).
% 
%% Convergence of 494_bus for different matrix reorderings

[A, b] = load_mat('494_bus.mat');

figure;
L_none = ichol(A);
[alpha, beta] = eigbounds(A, L_none);
resvec = chebyshev_g(A, @(x) L_none'\(L_none\x), b, alpha, beta, 1e-5);
semilogy(resvec)
hold on
%nested disection
p = dissect(A);
L_nds = ichol(A(p, p));
[alpha, beta] = eigbounds(A(p, p), L_nds);
resvec = chebyshev_g(A(p, p), @(x) L_nds'\(L_nds\x), b(p), alpha, beta, 1e-5);
semilogy(resvec)
%reverse cuthill mckee
p = symrcm(A);
L_rcm = ichol(A(p, p));
[alpha, beta] = eigbounds(A(p, p), L_rcm);
resvec = chebyshev_g(A(p, p), @(x) L_rcm'\(L_rcm\x), b(p), alpha, beta, 1e-5);
semilogy(resvec)
%minimum degree
p = amd(A);
L_md = ichol(A(p, p));
[alpha, beta] = eigbounds(A(p, p), L_md);
resvec = chebyshev_g(A(p, p), @(x) L_md'\(L_md\x), b(p), alpha, beta, 1e-5);
semilogy(resvec)
%col perm
p = colperm(A);
L_colp = ichol(A(p, p));
[alpha, beta] = eigbounds(A(p, p), L_colp);
resvec = chebyshev_g(A(p, p), @(x) L_colp'\(L_colp\x), b(p), alpha, beta, 1e-5);
semilogy(resvec)

grid on
ylabel('Relative residual norm');
xlabel('Iteration');
legend('Solver with no reordering', 'Nested disection reordering', 'Reverse Cuthill McKee', 'Minimum degree reordering', 'Colperm')
title('Convergence of Chebyshev solver');

figure;
subplot(3,2,1)
spy(L_none);
title('No reordering')
subplot(3,2,2)
spy(L_nds);
title('nested disection')
subplot(3,2,3)
spy(L_rcm);
title('rcm')
subplot(3,2,4)
spy(L_md);
title('min degree')
subplot(3,2,5)
spy(L_colp)
title('col perm')
%%
% 
%  We can see that the number of iterations for convergence reduces when
%  the matrix is reordered, with reverse Cuthill McKee reordering perform-
%  ing the best. 
% 

