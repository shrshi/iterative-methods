m = 20;
A = delsq(numgrid('S', m+2));
M = diag(diag(A));
rng(0);
b = rand(m*m,1)-.5;

% form symmetric matrix, so we can use eig on sparse matrix
% Z = inv(sqrtm(full(M)));
Z = inv(sqrt(M)); % works with diagonal M only
e = eig(speye(m*m)-Z*A*Z); % Z*A*Z is similar to inv(M)*A;
alpha = min(e);
beta  = max(e);
fprintf('alpha %f, beta %f\n', alpha, beta);

%relres1 = jacobi(A, b, 30);
relres2 = chebyshev_g(A, M, b, alpha, beta, 30);

clf
%semilogy(relres1)
%hold on
semilogy(relres2)
grid on
ylabel('Relative residual norm');
xlabel('Iteration');
