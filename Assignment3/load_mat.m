function [A, b] = load_mat(f)
    data = load(f);
    A = data.Problem.A;
    b = randn(size(A, 1), 1);
    b = b/norm(b);
end
    