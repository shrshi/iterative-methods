function resvec = chebyshev_g(A, M, b, alpha, beta, tol)
% resvec = chebyshev_g(A, M, b, alpha, beta, maxit)
%  Approximately solve  A x = b  by maxit steps of Chebyshev acceleration.
%
%  M can be a matrix or a preconditioning function
%   for example: chebyshev_g(A, @(x)precon(x,l,u), b, alpha, beta, maxit)
%
%  [alpha,beta] contains the spectrum of the iteration matrix
%
%  resvec is the relative residual norms at each step (excluding the first)

% Ref: Golub and van Loan

if isa(M, 'function_handle')
  mfun = M;
else
  mfun = @(x) M\x;
end

maxit = 1000;
resvec = zeros(maxit,1);

mu = 1 + 2*(1-beta)/(beta-alpha);
gamma = 2/(2-alpha-beta);

c0 = 1;
c1 = mu;
x0 = zeros(size(b));
x1 = mfun(b);
resvec(1) = norm(b-A*x1)/norm(b);

for k = 2:maxit
  z = mfun(b-A*x1);

  c2 = 2*mu*c1 - c0;
  x = x0 + 2*mu*c1/c2*(x1 - x0 + gamma*z);
  % x = x1 + z; % jacobi

  resvec(k) = norm(b-A*x)/norm(b);

  c0 = c1;
  c1 = c2;

  x0 = x1;
  x1 = x;
  if(resvec(k) <= tol)
      break
  end
end
resvec = resvec(1:k);
end
