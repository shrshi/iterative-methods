function [A, b, x0] = load_mat(f)
    data = load(f);
    A = data.Problem.A;
    b = randn(size(A, 1), 1);
    b = b/norm(b);
    x0 = zeros(size(A, 1), 1);
end
    