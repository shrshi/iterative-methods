%% Preconditioned Conjugate Gradient (PCG) method
%% Sparse matrix : bcsstk01
% 48X48 SPD 400 NNZs K(A) ~ 10^6

figure;
[A, b, x0] = load_mat('bcsstk01.mat');

% incomplete cholesky preconditioner
L = ichol(A);
% condition number after preconditioning
fprintf('Condition number with IC preconditioning = %e\n\n', cond((L\A)/L'));
[resvec, x, flag] = mypcg(@(x) L'\(L\x), 1e-8, 200, A, b, x0);
subplot(1,4,1)
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');
title('IC');

% sgs preconditioner
D = diag(diag(A));
Dinv = inv(D);
E = -tril(A, -1);
F = -triu(A, 1);
% condition number after preconditioning
fprintf('Condition number with SGS preconditioning = %e\n\n', cond( ((speye(size(A)) - E*Dinv)\A)/(D-F) ));
[resvec, x, flag] = mypcg(@(x) (D-F)\((speye(size(A)) - E*Dinv)\x), 1e-8, 200, A, b, x0);
subplot(1,4,2)
semilogy(resvec)
grid on
xlabel('Iteration');
title('SGS');

% two steps of sgs iterative method
M = (D-E)*Dinv*(D-F);
A_tilde = M/(2*speye(size(A)) - (M\A));
% condition number after preconditioning
fprintf('Condition number with 2-step SGS preconditioning = %e\n\n', cond(A_tilde));
[resvec, x, flag] = mypcg(@(x) (2*speye(size(A)) - M\A)*(M\x), 1e-8, 200, A, b, x0);
subplot(1,4,3)
semilogy(resvec)
grid on
xlabel('Iteration');
title('2-SGS');

% chebyshev acceleration with IC
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-8);
subplot(1,4,4)
semilogy(resvec)
grid on
xlabel('Iteration');
title('Chebyshev');

%%
% 
%  We can see that preconditoning makes A better conoditioned. PCG results
%  in faster convergence than Chebyshev acceleration with IC
% 
%% Verifying PCG implementation
% Using the previous matrix with IC preconditioning, we can verify the
% implementation of PCG by comparing it with MATLAB's |pcg| function
figure;
[resvec, x, flag] = mypcg(@(x) L'\(L\x), 1e-8, 200, A, b, x0);
subplot(1,2,1)
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');
title('mypcg');

[x,flag,relres,iter,resvec] = pcg(A, b, 1e-8, 200, @(x) L'\(L\x));
subplot(1,2,2)
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');
title('pcg');
%% Sparse matrix : bcsstk02
% 66X66 SPD 4356 NNZs K(A) ~ 10^4

figure;
[A, b, x0] = load_mat('bcsstk02.mat');
% incomplete cholesky preconditioner
L = ichol(A);
% condition number after preconditioning
fprintf('Condition number with IC preconditioning = %e\n\n', cond((L\A)/L'));
[resvec, x, flag] = mypcg(@(x) L'\(L\x), 1e-8, 100, A, b, x0);
subplot(1,4,1)
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');
title('IC');

% sgs preconditioner
D = diag(diag(A));
Dinv = inv(D);
E = -tril(A, -1);
F = -triu(A, 1);
% condition number after preconditioning
fprintf('Condition number with SGS preconditioning = %e\n\n', cond( ((speye(size(A)) - E*Dinv)\A)/(D-F) ));
[resvec, x, flag] = mypcg(@(x) (D-F)\((speye(size(A)) - E*Dinv)\x), 1e-8, 100, A, b, x0);
subplot(1,4,2)
semilogy(resvec)
grid on
xlabel('Iteration');
title('SGS');

% two steps of sgs iterative method
M = (D-E)*Dinv*(D-F);
A_tilde = M/(2*speye(size(A)) - (M\A));
% condition number after preconditioning
fprintf('Condition number with 2-step SGS preconditioning = %e\n\n', cond(A_tilde));
[resvec, x, flag] = mypcg(@(x) (2*speye(size(A)) - M\A)*(M\x), 1e-8, 100, A, b, x0);
subplot(1,4,3)
semilogy(resvec)
grid on
xlabel('Iteration');
title('2-SGS');

% chebyshev acceleration with IC
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-8);
subplot(1,4,4)
semilogy(resvec)
grid on
xlabel('Iteration');
title('Chebyshev');
%%
% 
%  This matrix is dense, and IC is very close to complete Cholesky
%  decomposition. PCG with IC converges in a single step (in fact, the 
%  preconditioned matrix is unitary since the condition number becomes 1), 
%  and Chebyshev acceleration with IC converges in 2 steps. Also,
%  preconditioning with SGS seems to worsen convergence.
% 
%% Sparse matrix : 494_bus
% 494X494 SPD 1666 NNZs K(A) ~ 10^6

figure;
[A, b, x0] = load_mat('494_bus.mat');

% incomplete cholesky preconditioner
L = ichol(A);
% condition number after preconditioning
fprintf('Condition number with IC preconditioning = %e\n\n', cond((L\A)/L'));
[resvec, x, flag] = mypcg(@(x) L'\(L\x), 1e-5, 300, A, b, x0);
subplot(1,4,1)
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');
title('IC');

% sgs preconditioner
D = diag(diag(A));
Dinv = inv(D);
E = -tril(A, -1);
F = -triu(A, 1);
% condition number after preconditioning
fprintf('Condition number with SGS preconditioning = %e\n\n', cond( ((speye(size(A)) - E*Dinv)\A)/(D-F) ));
[resvec, x, flag] = mypcg(@(x) (D-F)\((speye(size(A)) - E*Dinv)\x), 1e-5, 300, A, b, x0);
subplot(1,4,2)
semilogy(resvec)
grid on
xlabel('Iteration');
title('SGS');

% two steps of sgs iterative method
M = (D-E)*Dinv*(D-F);
A_tilde = M/(2*speye(size(A)) - (M\A));
% condition number after preconditioning
fprintf('Condition number with 2-step SGS preconditioning = %e\n\n', cond(A_tilde));
[resvec, x, flag] = mypcg(@(x) (2*speye(size(A)) - M\A)*(M\x), 1e-5, 300, A, b, x0);
subplot(1,4,3)
semilogy(resvec)
grid on
xlabel('Iteration');
title('2-SGS');

% chebyshev acceleration with IC
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-5);
subplot(1,4,4)
semilogy(resvec)
grid on
xlabel('Iteration');
title('Chebyshev');
%% Sparse matrix : 662_bus
% 662X662 SPD 2474 NNZs K(A) ~ 10^5

figure;
[A, b, x0] = load_mat('662_bus.mat');

% incomplete cholesky preconditioner
L = ichol(A);
% condition number after preconditioning
fprintf('Condition number with IC preconditioning = %e\n\n', cond((L\A)/L'));
[resvec, x, flag] = mypcg(@(x) L'\(L\x), 1e-8, 200, A, b, x0);
subplot(1,4,1)
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');
title('IC');

% sgs preconditioner
D = diag(diag(A));
Dinv = inv(D);
E = -tril(A, -1);
F = -triu(A, 1);
% condition number after preconditioning
fprintf('Condition number with SGS preconditioning = %e\n\n', cond( ((speye(size(A)) - E*Dinv)\A)/(D-F) ));
[resvec, x, flag] = mypcg(@(x) (D-F)\((speye(size(A)) - E*Dinv)\x), 1e-8, 200, A, b, x0);
subplot(1,4,2)
semilogy(resvec)
grid on
xlabel('Iteration');
title('SGS');

% two steps of sgs iterative method
M = (D-E)*Dinv*(D-F);
A_tilde = M/(2*speye(size(A)) - (M\A));
% condition number after preconditioning
fprintf('Condition number with 2-step SGS preconditioning = %e\n\n', cond(A_tilde));
[resvec, x, flag] = mypcg(@(x) (2*speye(size(A)) - M\A)*(M\x), 1e-8, 200, A, b, x0);
subplot(1,4,3)
semilogy(resvec)
grid on
xlabel('Iteration');
title('2-SGS');

% chebyshev acceleration with IC
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-8);
subplot(1,4,4)
semilogy(resvec)
grid on
xlabel('Iteration');
title('Chebyshev');
%% Sparse matrix : bcsstk15
% 3948X3948 SPD 117816 NNZs K(A) ~ 10^9

figure;
[A, b, x0] = load_mat('bcsstk15.mat');

% incomplete cholesky preconditioner
% since this matrix has a non positive pivot, we use a diagonal shift to
% make M = A + alpha*(diag(diag(A)) diagonally dominant
alph = max(sum(abs(A),2)./diag(A))-2;
L = ichol(A, struct('diagcomp', alph));
% condition number after preconditioning
fprintf('Condition number with IC preconditioning = %e\n\n', cond((L\A)/L'));
[resvec, x, flag] = mypcg(@(x) L'\(L\x), 1e-4, 500, A, b, x0);
subplot(1,4,1)
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');
title('IC');

% sgs preconditioner
D = diag(diag(A));
Dinv = inv(D);
E = -tril(A, -1);
F = -triu(A, 1);
% condition number after preconditioning
fprintf('Condition number with SGS preconditioning = %e\n\n', cond( ((speye(size(A)) - E*Dinv)\A)/(D-F) ));
[resvec, x, flag] = mypcg(@(x) (D-F)\((speye(size(A)) - E*Dinv)\x), 1e-4, 500, A, b, x0);
subplot(1,4,2)
semilogy(resvec)
grid on
xlabel('Iteration');
title('SGS');

% two steps of sgs iterative method is computationally expensive for this
% matrix and does not finish running in the allotted time

% chebyshev acceleration with IC
[alpha, beta] = eigbounds(A, L);
resvec = chebyshev_g(A, @(x) L'\(L\x), b, alpha, beta, 1e-4);
subplot(1,4,4)
semilogy(resvec)
grid on
xlabel('Iteration');
title('Chebyshev');
%%
% 
%  Chebyshev does not converge to the required tolerance in 1000
%  iterations. PCG with SGS preconditioning performs much better
% 
% 
%  In all the above plots, it is clear that 2 step SGS converges much
%  faster (approximately half the number of steps) than SGS, even if the 
%  condition number of the matrix with 2 step SGS preconditioning is worse. 
% 