function [resvec, x, flag] = mypcg(precon, threshold, maxiters, A, b, x0)
    r = b - A*x0;
    p = precon(r);
    x = x0;
    resvec = zeros(maxiters);
    flag = 1;
    for k=1:maxiters
        z = precon(r);
        t = A*p;
        alpha = dot(r, z)/dot(p, t);
        x_n = x + alpha*p;
        r_n = r - alpha*t;
        z_n = precon(r_n);
        beta = dot(r_n, z_n)/dot(r, z);
        p = z_n + beta*p;
        
        resvec(k) = norm(r_n);
        if((norm(b - A*x_n)/norm(b - A*x0)) <= threshold)
            flag=0;
            break;
        end
        x = x_n;
        r = r_n;
    end
    x = x_n;
    resvec = resvec(1:k);
end