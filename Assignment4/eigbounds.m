function [alpha, beta] = eigbounds(A, L)
    G = speye(size(A, 1)) - L'\(L\A);
    beta = eigs(G, 1, 'lr');
    alpha = eigs(G, 1, 'sr');
end
