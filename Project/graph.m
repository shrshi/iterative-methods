function M = graph(A, k)
    n = size(A, 1);
    D = diag(sum(A, 2));
    L = D - A;
    disp(L);
    [V, lmbd] = eig(L);
    disp(V);
    V = V(:, 1:k);
    idx = kmeans(V, k);

    [out, sorted_idx] = sort(idx);
    P = eye(n);
    P = P(sorted_idx, :);

    [C, ia, ic] = unique(idx);
    counts = accumarray(ic, 1);
    start = 1;
    sub_M = cell(k, 1);
    for c=1:length(counts)
        sub_idx = sorted_idx(start:start+counts(c)-1);
        sub_P = P(start:start+counts(c)-1, :);
        sub_M{c} = sub_P*A*sub_P';
    end
    M = sparse(blkdiag(sub_M{:}));
end