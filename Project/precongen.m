function M_tau = precongen(X, A, q)
    mask = X*X.';
    mask = sqrt(diag(mask) + diag(mask)' - 2*mask);

    tau = quantile(mask, q, 'all');
    tau = tau(1);

    mask = (mask <= tau);
    M_tau = sparse(A.*mask);
end