n = 1000;
X = rand(n, 3);
b = rand(n, 1);
%% chol/ichol preconditioner with dcr
sp = [0.6 0.8 0.995];
sigs = 0.01:0.01:0.11;
iter = zeros(size(sigs, 2), size(sp, 2));
flag = zeros(size(sigs, 2), size(sp, 2));

for i=1:size(sigs, 2)
    A = matgen(X, sigs(i));
    for j=1:size(sp, 2)
        M = dcr(A, sp(j));
        R = ichol(M);
        [x,flag(i, j),relres,iter(i, j),resvec] = pcg(A, b, 1e-10, 5000, @(x) R\(R'\x));
    end
end
figure;
plot(sigs, iter(:, 1), 'r', sigs, iter(:, 2), 'b', sigs, iter(:, 3), 'g');
xlabel('Bandwidth');
ylabel('Number of iterations to convergence');
legend('Sparsity = 0.6', 'Sparsity = 0.8', 'Sparsity = 0.995');

% figure;
% plot(sigs, iters);
% ylabel('Number of iterations to convergence');
% xlabel('sigma');
% figure;
% semilogy(resvec)
% grid on
% ylabel('Residual norm');
% xlabel('Iteration');
% figure;
% semilogy(sigs, conds(:, 1), sigs, conds(:, 2));
% legend('No preconditioning', 'Preconditioning');
% grid on
% ylabel('Logarithm of condition number');
% xlabel('Bandwidth of Gaussian (sigma)');

%% chol/ichol preconditioner with spectral
n_clusts = [10, 15, 20];
sigs = 0.01:0.01:0.29;
iter = zeros(size(sigs, 2), size(n_clusts, 2));
flag = zeros(size(sigs, 2), size(n_clusts, 2));

conds = zeros(size(sigs, 2), size(n_clusts, 2)+1);

for i=1:size(sigs, 2)
    A = matgen(X, sigs(i));
    for j=1:size(n_clusts, 2)
        M = spectral_clustering(A, n_clusts(j));
        d = colperm(M);
        R = chol(M(d,d));
        [x,flag(i, j),relres,iter(i, j),resvec] = pcg(A(d,d), b(d), 1e-10, 5000, @(x) R\(R'\x));
        conds(i, j) = cond((R'\A)/R);
    end
    conds(i, j+1) = cond(A);
end
figure;
semilogy(sigs, conds(:, 1), 'r', sigs, conds(:, 2), 'b', sigs, conds(:, 3), 'g', sigs, conds(:, 4), 'c');
xlabel('Bandwidth');
ylabel('Logarithm of condition number');
legend('nclust = 10', 'nclust = 15', 'nclust = 20');

% figure;
% plot(sigs, iters);
% ylabel('Number of iterations to convergence');
% xlabel('sigma');
% figure;
% semilogy(resvec)
% grid on
% ylabel('Residual norm');
% xlabel('Iteration');
% figure;
% semilogy(sigs, conds(:, 1), sigs, conds(:, 2));
% legend('No preconditioning', 'Preconditioning');
% grid on
% ylabel('Logarithm of condition number');
% xlabel('Bandwidth of Gaussian (sigma)');

%% chol dcr g compare

nclust = 15;
sp = 0.9;
sigs = 0.03;
conds = zeros(size(sigs, 2), 3);

for i=1:size(sigs, 2)
    A = matgen(X, sigs(i));
    M_dcr = dcr(A, sp);
    M_g = spectral_clustering(A, nclust);
    R_dcr = chol(M_dcr);
    R_g = chol(M_g);
    conds(i, 1) = cond(A);
    conds(i, 2) = cond((R_dcr'\A)/R_dcr);
    conds(i, 3) = cond((R_g'\A)/R_g);
    [x_dcr,flag_dcr,relres_dcr,iter_dcr,resvec_dcr] = pcg(A, b, 1e-10, 5000, @(y) R_dcr\(R_dcr'\y));
    [x_g,flag_g,relres_g,iter_g,resvec_g] = pcg(A, b, 1e-10, 5000, @(y) R_g\(R_g'\y));
end
% figure;
% semilogy(sigs, conds(:, 1), 'r', sigs, conds(:, 2), 'b', sigs, conds(:, 3), 'g');
% xlabel('Bandwidth');
% ylabel('Logarithm of condition number');
% legend('No precon', 'Precon with Mdcr', 'Precon with Mg');

figure;
semilogy(1:1:(iter_dcr+1), resvec_dcr, 'g');
hold on
semilogy(1:1:(iter_g+1), resvec_g, 'r');
hold off;
grid on
ylabel('Residual norm');
xlabel('Iteration');
legend('dcr', 'g');

%% FSAI

nclust = 11;
sp = 0.995;
q = 150;
sigs = 0.01:0.01:0.29;
iter = zeros(size(sigs, 2), 3);
conds = zeros(size(sigs, 2), 4);

for i=1:size(sigs, 2)
    A = matgen(X, sigs(i));
    M_tau = precongen(X, A, q);
    %d = symrcm(M_tau);
    U_tau = fsai(A, M_tau);
    M_dcr = dcr(A, sp);
    d = symrcm(M_dcr);
    U_dcr = fsai(A(d,d), M_dcr(d,d));
    M_g = spectral_clustering(A, nclust);
    d = amd(M_g);
    U_g = fsai(A(d,d), M_g(d,d));
    %[x,flag,relres,iter(i, 1),resvec] = pcg(A, b, 1e-10, 5000, @(x) U_tau*U_tau'*x);
    %[x,flag,relres,iter(i, 2),resvec] = pcg(A(d,d), b(d), 1e-10, 5000, @(x) U_dcr*U_dcr'*x);
    %[x,flag,relres,iter(i, 3),resvec] = pcg(A, b, 1e-10, 5000, @(x) U_g*U_g'*x);
    conds(i, 1) = cond(A);
    conds(i, 2) = cond(U_tau'*A*U_tau);
    conds(i, 3) = cond(U_dcr'*A*U_dcr);
    conds(i, 4) = cond(U_g'*A*U_g);
end
figure;
semilogy(sigs, conds(:, 1), 'r', sigs, conds(:, 2), 'g', sigs, conds(:, 3), 'b', sigs, conds(:, 4), 'c');
ylabel('Logarithm of condition number');
xlabel('sigma');
legend('No precon', 'Mtau', 'Mdcr', 'Mg');
% figure;
% semilogy(resvec)
% grid on
% ylabel('Residual norm');
% xlabel('Iteration');


%% Chebyshev precon

sigs = 0.01:0.01:0.13;
iter = zeros(size(sigs));
conds = zeros(size(sigs));
flag = zeros(size(sigs));
ks = 100:100:1800;

k = 100;
sigma = 0.05;

% for i=1:size(sigs, 2)
%     disp(sigs(i));
%     A = matgen(X, sigs(i));
%     beta = eigs(A, 1, 'lm');
%     alpha = eigs(A, 1, 'sm');
%     [x,flag(i),relres,iter(i),resvec] = pcg(A, b, 1e-10, 5000, @(y) chebyshev_precon(A, y, alpha, beta, k, 1e-10)); 
% end
% figure;
% plot(sigs, iter);
% ylabel('Number of iterations to convergence');
% xlabel('sigma');

iter = zeros(size(ks));
flag = zeros(size(ks));
A = matgen(X, sigma);
beta = eigs(A, 1, 'lm');
alpha = eigs(A, 1, 'sm');
for i=1:size(ks, 2)
    disp(ks(i));
    [x,flag(i),relres,iter(i),resvec] = pcg(A, b, 1e-10, 5000, @(y) chebyshev_precon(A, y, alpha, beta, ks(i), 1e-10));     
end
plot(ks, iter);
xlabel('Polynomial order');
ylabel('Number of iterations to convergence');
% figure;
% semilogy(resvec);
% grid on
% ylabel('Residual norm');
% xlabel('Iteration');

% figure;
% semilogy(sigs, conds(:, 1), sigs, conds(:, 2));
% legend('No preconditioning', 'Preconditioning');
% grid on
% ylabel('Logarithm of condition number');
% xlabel('Bandwidth of Gaussian (sigma)');
%% PCG iterations for cholesky
sigs = 0.01:0.01:0.29;
iters = zeros(size(sigs, 2), 3);
flags = zeros(size(sigs, 2), 3);

q = 4;
for i=1:size(sigs, 2)
    disp(sigs(i));
    A = matgen(X, sigs(i));
    %sparse cholesky with M_dcr
    M = dcr(A, 0.98);
    R = chol(M);
    [x, flags(i, 2), relres, iters(i, 2), resvec] = pcg(A, b, 1e-10, 2000, @(x) R\(R'\x));
    %sparse cholesky with M_g
    M = spectral_clustering(A, 9);
    R = chol(M);
    [x, flags(i, 3), relres, iters(i, 3), resvec] = pcg(A, b, 1e-10, 2000, @(x) R\(R'\x));
end
figure;
plot(sigs, iters(:,1), 'r', sigs, iters(:, 2), 'b', sigs, iters(:, 3), 'g');
ylabel('Number of iterations to convergence');
xlabel('sigma');

%% Conclusion
sigma = 0.05;
A = matgen(X, sigma);
% no precon
[x, flag_noprecon, relres, iter_noprecon, resvec_noprecon] = pcg(A, b, 1e-10, 2000);
% chol precon
sp = 0.993;
M_dcr = dcr(A, sp);
R = chol(M_dcr);
[x, flag_chol, relres, iter_chol, resvec_chol] = pcg(A, b, 1e-10, 5000, @(y) R\(R'\y));
% FSAI precon
q = 150;
M_tau = precongen(X, A, q);
U_tau = fsai(A, M_tau);
[x,flag_fsai,relres,iter_fsai,resvec_fsai] = pcg(A, b, 1e-10, 2000, @(x) U_tau*U_tau'*x);
% chebyshev
k = 2500;
beta = eigs(A, 1, 'lm');
alpha = eigs(A, 1, 'sm');
[x,flag_cheb,relres,iter_cheb,resvec_cheb] = pcg(A, b, 1e-10, 2000, @(y) chebyshev_precon(A, y, alpha, beta, k, 1e-10));

semilogy(1:(iter_cheb+1), resvec_cheb, 1:(iter_chol+1), resvec_chol, 1:(iter_fsai+1), resvec_fsai, 1:(iter_noprecon+1), resvec_noprecon);
xlabel('Iteration');
ylabel('Residual norm');
legend('Chebyshev', 'Cholesky', 'FSAI', 'No precon');