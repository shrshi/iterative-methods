function U = fsai(A, pattern)
% U = fsai(A, pattern)
%  Factorized sparse approximate inverse; symmetric version.
%  Returns upper triangular factor U such that inv(A) ~= U*U' and U'AU ~= I.
%  If A is SPD, U is also approximately the inverse of the upper triangular
%  Cholesky factor.

% E. Chow, 04-23-2012   Created from fsai4.m

n = size(A,2);
rfull = spalloc(n,1,1);
U = triu(pattern);

% compute U

for col = 1:n

    if (mod(col,500) == 0) fprintf('%d ', col), end;

    [i,j] = find(U(:,col));

    % note i is an index set
    Ahat = A(i,i);

    rfull(col) = 1;
    rhs = rfull(i);

    m = Ahat \ rhs;
    U(:,col) = sparse(i,j,full(m),n,1);

    rfull(col) = 0;
end

if (n >= 500) fprintf('\n'), end;
if sum(diag(U) <= 0), error('zero or negative on diagonal of U'); end
D = 1./sqrt(diag(U));
D = spdiags(D,0,n,n);
U = U * D;
