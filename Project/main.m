n = 1000;
X = rand(n, 3);
b = rand(n, 1);
%%
sigs = 0.01:0.01:0.5;
for i=1:size(sigs, 2)
    A = matgen(X, sigs(i));
    [R, flag] = chol(A);
    if flag ~= 0
        disp(sigs(i));
        break;
    end
end
%%
sigs = 0.01:0.01:0.4;
alphas = zeros(size(sigs));
alphas_m = zeros(size(sigs));
smallest = zeros(size(sigs));
for i=1:size(sigs, 2)
    A = matgen(X, sigs(i));
    alphas(i) = eigs(A, 1, 'sr');
    alphas_m(i) = eigs(A, 1, 'sm');
    e = eig(A);
    smallest(i) = e(1);
end
% figure;
% plot(sigs, alphas);
% ylabel('Smallest real eigenvalue');
% xlabel('Bandwidth');
% figure;
% plot(sigs, alphas_m);
% ylabel('Smallest magnitude eigenvalue');
% xlabel('Bandwidth');
%%
sigs = 0.01:0.01:0.1;
iters = zeros(size(sigs));
for i=1:size(sigs, 2)
    A = matgen(X, sigs(i));
    %M = precongen(X, A, 150);
    M = dcr(A, 0.7);
    U = fsai(A, M);
    [x,flag,relres,iter,resvec] = pcg(A, b, 1e-8, 1000, @(x) U*U'*x);
    iters(i) = iter;
end
figure;
plot(sigs, iters);
ylabel('Number of iterations to convergence');
xlabel('sigma');
figure;
semilogy(resvec)
grid on
ylabel('Residual norm');
xlabel('Iteration');

%%
sigs = 0.01:0.01:0.1;
conds = zeros(size(sigs, 2), 2);
for i = 1:size(sigs, 2)
    A = matgen(X, sigs(i));
    M = dcr(A, 0.7);
    U = fsai(A, M);
    conds(i, 1) = cond(A);
    conds(i, 2) = cond(U'*A*U);
end
semilogy(sigs, conds(:, 1), sigs, conds(:, 2));
legend('No preconditioning', 'Preconditioning');
grid on
ylabel('Logarithm of condition number');
xlabel('Bandwidth of Gaussian (sigma)');
%%
sigma = 0.1;
q = 50:5:250;
iters = zeros(size(q));
nnzs = zeros(size(q));
for i = 1:size(q, 2)
    A = matgen(X, sigma);
    M = precongen(X, A, q(i));
    nnzs(i) = nnz(M);
    U = fsai(A, M);
    [x,flag,relres,iter,resvec] = pcg(A, b, 1e-8, 1000, @(x) U*U'*x);
    iters(i) = iter;
end
figure;
plot(q, iters);
xlabel('Quantile');
ylabel('Number of iterations to convergence');
figure;
plot(q, nnzs);
xlabel('Quantile');
ylabel('Number of nonzeros in M');
%%
sigma = 0.1;
A = matgen(X, sigma);
for q=2:1:150
    M = precongen(X, A, q);
    [R, flag] = chol(M);
    if flag ~= 0
        disp(q);
        break;
    end
end
%%
sigma = 100;
A = matgen(X, sigma);
M = dcr(A, 0.95);
R = chol(A);
%%
sigma = 100;
A = matgen(X, sigma);
[M, A] = spectral_clustering(A, 9);
L = chol(A);
cond(A)
cond((L\A)/L')
%% chebyshev
sigma = 0.2;
A = matgen(X, sigma);
beta = eigs(A, 1, 'lr');
alpha = eigs(A, 1, 'sr');
k = 10;
[x,flag,relres,iter,resvec] = pcg(A, b, 1e-11, 1000, @(y) chebyshev_precon(A, y, alpha, beta, k, 1e-10));
semilogy(resvec)