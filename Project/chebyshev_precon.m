function [x, resvec] = chebyshev_precon(A, b, alpha, beta, k, tol)
    x0 = zeros(size(A, 2), 1);
    r = b - A*x0;
    theta = (alpha + beta)/2;
    delta = (beta - alpha)/2;
    sigma1 = theta/delta;
    rho = 1/sigma1;
    d = r/theta;
    x = x0;
    resvec = zeros(10000, 1);
    i=1;
    if k==0
        resnrm = 1;
        while resnrm > tol
            x = x + d;
            r = r - A*d;
            rho_n = 1/((2*sigma1) - rho);
            d = rho_n*rho*d + ((2*rho_n*r)/delta);
            resnrm = norm(A*x - b);
            resvec(i) = resnrm;
            i = i + 1;
        end
        resvec = resvec(1:i);
    else
        for ord = 1:k
            x = x + d;
            r = r - A*d;
            rho_n = 1/((2*sigma1) - rho);
            d = rho_n*rho*d + ((2*rho_n*r)/delta);
        end
    end
end