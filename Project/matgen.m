function A = matgen(X, sigma)
    A = X*X.';
    A = diag(A) + diag(A)' - 2*A;
    A = exp((-1/(2*sigma^2)) * A);
end