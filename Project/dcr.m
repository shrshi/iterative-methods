function M = dcr(A, sp)
    n = size(A, 1);
    mask = rand(n);
    
    B_logical = (mask > sp);
    R_logical = (mask <= sp);
    R_logical = R_logical - diag(diag(R_logical));
    R = A.*R_logical;
    
    M = A.*B_logical;
    M(logical(eye(n))) = diag(A) + sum(R, 2);
    M = sparse(M);
end