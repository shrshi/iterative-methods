function val = F(u, h, n, lambda)
    for i=1:n
        val(i, 1) = (lambda*h*h*exp(u(i, 1))) - 2*u(i, 1);
        if(i ~= n)
            val(i, 1) = val(i, 1) + u(i+1, 1);
        end
        if(i ~= 1)
            val(i, 1) = val(i, 1) + u(i-1, 1);
        end
    end
end