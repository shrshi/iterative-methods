function [inner_iters, norm_est, x] = inexact_Newton(x0, tol, eta, eps, n, h, lambda)
maxiters = 500;
norm_est = zeros(maxiters, 1);
inner_iters = zeros(maxiters, 2);

x = x0;
for k=1:maxiters
    [del_x, flag, relres, iter] = gmres(@(w) (F(x + eps*w, h, n, lambda) - F(x, h, n, lambda))/eps, -F(x, h, n, lambda), n, eta, n);
    inner_iters(k, :) = iter;
    x = x + del_x;
    norm_est(k) = norm(F(x, h, n, lambda))/norm(F(x0, h, n, lambda));
    if(norm(F(x, h, n, lambda)) < tol)
        break;
    end
end
inner_iters = inner_iters(1:k, :);
norm_est = norm_est(1:k);
end