function jacobian = J(u, h, n, lambda)
    jacobian = zeros(n, n);
    for i=1:n
        jacobian(i, i) = lambda*h*h*exp(u(i, 1)) - 2;
        if(i ~= n)
            jacobian(i, i+1) = 1;
        end
        if(i ~= 1)
            jacobian(i, i-1) = 1;
        end
    end
end