%% Assignment 5
%% Function that returns F(x)
% <include>F.m</include>
%% Function that returns Jacobian
% <include>J.m</include>
%% mesh parameters
n = 1000;
h = 1.0/n;
lambda = 1;
%% Newton's method
% <include>Newtons_method.m</include>
%% Varying initial guess for Newton's method
%
tol = 1e-10;
x0 = 3.9*ones(n, 1);
[x, norm_est] = Newtons_method(x0, tol, n, h, lambda);
figure;
plot(norm_est);
title("Quadratic convergence");
xlabel("Iteration")
ylabel("Relative norm");
% From the above plot, we can see that the convergence is indeed quadratic
% near the solution
%% Solution of differential equation
figure;
plot(x);
title("Solution from Newton's method");
xlabel("Coordinate i");
ylabel("u(i)");
% We get the same solution with MATLAB's fsolve
%% Solution from fsolve
figure;
plot(fsolve(@(u)F(u, h, n, lambda), x0));
title("Solution from fsolve");
xlabel("Coordinate i");
ylabel("u(i)");
% On varying the initial guess, divergence is observed for larger values of
% init guess and very quick convergence of values of guess very close to
% the solution
%% Rapid convergence closer to the solution
x0 = 0.1*ones(n, 1);
[x, norm_est] = Newtons_method(x0, tol, n, h, lambda);
figure;
plot(norm_est);
title("Convergence within 2 steps");
xlabel("Iteration");
ylabel("Relative norm");

% In fact, Newton's method converges to the solution within two steps even
% for smaller tolerance and larger values of n
%% No reduction in convergence rate for for higher tolerance
tol = 1e-13;
[x, norm_est] = Newtons_method(x0, tol, n, h, lambda);
figure;
plot(norm_est);
title("Convergence within 2 steps for tol=1e-13");
xlabel("Iteration");
ylabel("Relative norm");

%% No effect of mesh size on convergence rate
n = 10000;
h = 1.0/n;
x0 = 0.1*ones(n, 1);
[x, norm_est] = Newtons_method(x0, tol, n, h, lambda);
figure;
plot(norm_est);
title("Convergence within 2 steps for n=10000");
xlabel("Iteration");
ylabel("Relative norm");
%% Inexact Newton's method
% <include>Newtons_method.m</include>
%% Convergence for inexact Newton's method
n = 1000;
h = 1.0/n;

x0 = 0.5*ones(n, 1);
tol = 1e-14;
eps = 0.00001;

eta = 0.0001;
[inner_iters, norm_est, x] = inexact_Newton(x0, tol, eta, eps, n, h, lambda);
figure;
plot(norm_est);
title("Convergence for eta = 0.001");
xlabel("Iteration")
ylabel("Relative norm");

%% Varying eta for inexact Newton
etas=[0.1 0.01 0.001 0.0001 0.00001];
% across all values of eta, the number of outer iterations in GMRES remains
% constant (=1). However the number of inner iterations of GMRES, which corresponds
% to the number of restarts increases as the number of Newton iterations.
figure;
for e=1:length(etas)
    [inner_iters, norm_est, x] = inexact_Newton(x0, tol, etas(e), eps, n, h, lambda);
    steps(e) = length(norm_est);
    plot(inner_iters(:, 1), 'DisplayName', strcat('GMRES outer loop, eta = ', num2str(etas(e))));
    hold on;
    plot(inner_iters(:, 2), 'DisplayName', strcat('GMRES outer loop, eta = ', num2str(etas(e))));
    hold on;
end
legend;
title("GMRES iterations");

%% Number of steps to convergence for different values of eta
% On increasing eta, the number of Newton convergence steps decreases.
figure;
semilogx(etas, steps);
title("Number of steps to convergence")
xlabel("eta");
ylabel("Number of steps");

% The best value of eta is the largest value of eta after which there is
% not reduction in the convergence rate. We want to choose the largest
% value since GMRES itself becomes farther from convergence for smaller
% values of eta.
% Thus, the best value of eta is 0.0001
