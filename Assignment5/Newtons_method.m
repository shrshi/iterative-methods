function [x, norm_est] = Newtons_method(x0, tol, n, h, lambda)
maxiters = 100;
norm_est = zeros(maxiters, 1);

x = x0;
for k=1:maxiters
    del_x = -J(x, h, n, lambda) \ F(x, h, n, lambda);
    x = x + del_x;
    norm_est(k) = norm(F(x, h, n, lambda))/norm(F(x0, h, n, lambda));
    if(norm(F(x, h, n, lambda)) < tol)
        break;
    end
end
norm_est = norm_est(1:k);
end