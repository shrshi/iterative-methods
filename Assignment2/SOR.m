function [iters, residuals, x] = SOR(w, A, b, x0)

    tol = 0.01;
    residuals = zeros(10000, 1);
    
    M = diag(diag(A)) + w*tril(A, -1);
    N = -w*triu(A, 1) + (1.0-w)*diag(diag(A));
    
    err = norm(A*x0 - b);
    residuals(1) = err;
    
    x = x0;
    L = tril(A);
    U = triu(A, 1);
    iters = 2;
    while err > tol
%         x_new = M\(N*x + w*b);
        x_new = L\(b - U*x);
        x_new = w*x_new + (1-w)*x;
        err = norm(A*x_new - b);
        residuals(iters) = err;
        x = x_new;
        iters = iters + 1;
%         disp(err);
    end
end