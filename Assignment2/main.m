%% room specs
len = 10;
breadth = 6;
T = [310 330 300 320];
h = 1;
m = len/h - 1;
n = breadth/h - 1;

%% generating A
[A, b] = generate_A(h, n, m, T);
%% part 1
% initialisation of solution for SOR
rng(0,'twister');
x0 = 30.*rand(length(b), 1) + 300;

% optimal w
% w = 1.7;
% [iters, residuals, x] = SOR(w, A, b, x0);
% x(fix(length(x)/2))
ws = 0.1:0.05:1.9;
iterations = zeros(length(ws), 1);
i = 1;
% diverges for w = 1.7, 1.8, 1.9
for w=ws
    [iters, residuals, x] = SOR(w, A, b, x0);
    iterations(i) = iters;
%     disp(i);
%     disp(iters);
%     disp(x(fix(length(x)/2)));
    i = i + 1;
end
plot(ws, iterations);
title("Plot to find optimal w for tol = 0.01");
xlabel("w");
ylabel("Number of iterations");
disp("w_opt = ");
[min_iter, pos] = min(iterations);
ws(pos)
disp("Number of iterations at w_opt = ")
min_iter

%% part 2

%permutation vector
perm = rb_permvec(n, m);x
A_rb = A(perm, perm);
% spy(A_rb);
b_rb = b(perm);

% initialisation of solution for SOR
rng(0,'twister');
y0 = 30.*rand(length(b), 1) + 300;

% optimal cw
% w = 1;
% [iters, residuals, y] = SOR(w, A_rb, b_rb, y0);
% x(perm) = y;
% x(fix(length(x)/2))
ws = 0.1:0.05:1.99;
iterations = zeros(length(ws), 1);
i = 1;
for w=ws
    [iters, residuals, y] = SOR(w, A_rb, b_rb, y0);
    x(perm) = y;
    iterations(i) = iters;
    disp(i);
    disp(iters);
    disp(x(fix(length(x)/2)));
    i = i + 1;
end
plot(ws, iterations);
title("Plot to find optimal w for tol = 0.01");
xlabel("w");
ylabel("Number of iterations");
disp("w_opt = ");
[min_iter, pos] = min(iterations);
ws(pos)
disp("Number of iterations at w_opt = ")
min_iter

%% part 3
% spy(A);
% w = 1.5;
% [rho, nrm] = spectral(w, A);
% rho
ws = 0.1:0.05:1.99;
rhos = zeros(length(ws), 1);
nrms = zeros(length(ws), 1);
i = 1;
for w=ws
    [rho, nrm] = spectral(w, A);
    rhos(i) = rho;
    nrms(i) = nrm;
%     disp(i);
%     disp(rho);
%     disp(nrm);
    i = i + 1;
end
%figure(1)
% plot(ws, rhos);
% title("Plot to find optimal w");
% xlabel("w");
% ylabel("rho(G)");
disp("w_opt = ");
[min_rho, pos] = min(rhos);
ws(pos)
disp("rho(G) at w_opt = ")
min_rho
