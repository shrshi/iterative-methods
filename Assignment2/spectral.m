function [rho, nrm] = spectral(w, A)
    M = w * tril( A, -1 ) + diag(diag( A ));
    N = -w * triu( A,  1 ) + ( 1.0 - w ) * diag(diag( A ));
    G = M\N;
    isequal(G*G.', G.'*G)
    spy(G);
%     rho = eigs(full(G), 1, "largestabs");
    rho = max(abs(eig(full(G))));
    nrm = norm(full(G));
end