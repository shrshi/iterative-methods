n = 50;
A = sprandsym(n, 0.1);
figure(3)
spy(A)

M = w * tril( A, -1 ) + diag(diag( A ));
N = -w * triu( A,  1 ) + ( 1.0 - w ) * diag(diag( A ));
G = M\N;
figure(4)
spy(G)

G = sprandsym(n, 0.1);
ws = 0.1:0.05:1.99;
rhos = zeros(length(ws), 1);
nrms = zeros(length(ws), 1);
i = 1;
for w=ws
    rhos(i) = max(abs(eig(full(G))));
    nrms(i) = norm(full(G));
    i = i + 1;
end
figure(1)
plot(ws, rhos);
title("Plot to find optimal w");
xlabel("w");
ylabel("rho(G)");
figure(2)
plot(ws, nrms);
title("Plot to find optimal w");
xlabel("w");
ylabel("norm(G)");
disp("w_opt = ");
[min_rho, pos] = min(rhos);
ws(pos)
disp("rho(G) at w_opt = ")
min_rho
disp("w_opt = ");
[min_nrm, pos] = min(nrms);
ws(pos)
disp("norm(G) at w_opt = ")
min_nrm