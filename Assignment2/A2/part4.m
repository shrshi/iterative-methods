function [rho] = part4(w, w_opt, beta)
    
    if w>w_opt
        rho = w-1;
    else
        rho = 1.0 - w + 0.5*w^2*beta^2 + w*beta*sqrt(1.0 - w + 0.25*w^2*beta^2);
    end    
end