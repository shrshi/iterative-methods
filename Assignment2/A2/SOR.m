function [iters, residuals, x] = SOR(w, A, b, x0)

    tol = 0.01;
    residuals = zeros(10000, 1);
    
    M = diag(diag(A)) + w*tril(A, -1);
    N = -w*triu(A, 1) + (1.0-w)*diag(diag(A));
    
    err = norm(A*x0 - b);
    residuals(1) = err;
    
    x = x0;
    iters = 1;
    while residuals(iters) > tol
        x = M\(N*x + w*b);
        iters = iters + 1;
        residuals(iters) = norm(A*x - b);
    end
end