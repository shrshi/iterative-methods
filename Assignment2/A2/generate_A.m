function [A, b] = generate_A(n, m, T)

b = zeros((n+2)*(m+2), 1);
row = zeros(((n+2)*(m+2)- 2*n - 2*m - 4)*5 + 2*n + 2*m + 4, 1);
col = zeros(((n+2)*(m+2) - 2*n - 2*m - 4)*5 + 2*n + 2*m + 4, 1);
data = zeros(((n+2)*(m+2) - 2*n - 2*m - 4)*5 + 2*n + 2*m + 4, 1);

idx = 1;
start = m+4;
while start <= n*(m+2)+2
    for j=start:(start+m-1)
        row(idx:idx+4) = j;
        col(idx:idx+4) = [j, j-1, j+1, j+m+2, j-m-2];
        data(idx:idx+4) = [4, -1, -1, -1, -1];
        idx = idx + 5;     
    end
    start = start + m + 2;
end

b(2:m+1) = T(1);
b(1:m+2:(n+1)*(m+2)+1) = T(2);
b((n+1)*(m+2)+2:(n+2)*(m+2)) = T(3);
b(m+2:m+2:(n+2)*(m+2)) = T(4);

nnzs = find(b); 
row(idx:length(row)) = nnzs;
col(idx:length(row)) = nnzs;
data(idx:length(row)) = 1;
A = sparse(row, col, data);
end