function [p] = rb_permvec(n, m)
    % permutation vector

    p = zeros((n+2)*(m+2), 1);
    idx = 1;
    j = 1;
    while j<=(n+2)*(m+2)
        p(idx) = j;
        if rem(j, 2*(m+2)) > 0 && rem(j, 2*(m+2)) <= m+2
            if rem(j, m+2) + 2 > m+2
                j = j + 3;
            else
                j = j + 2;
            end
        else
            if rem(j, m+2) == 0
                j = j + 1;
            else
                j = j + 2;
            end
        end
        idx = idx + 1;
    end
    j = 2;
    while j<=(n+2)*(m+2)
        p(idx) = j;
        if rem(j, 2*(m+2)) > 0 && rem(j, 2*(m+2)) <= m+2
            if rem(j, m+2) == 0
                j = j + 1;
            else
                j = j + 2;
            end
        else
            if rem(j, m+2) + 2 > m+2
                j = j + 3;
            else
                j = j + 2;
            end
        end
        idx = idx + 1;
    end
end