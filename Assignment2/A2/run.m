%% room specs
len = 10;
breadth = 6;
T = [310 330 300 320];
h = 1;
m = len/h - 1;
n = breadth/h - 1;

%% generating A
[A, b] = generate_A(n, m, T);
%% part 1
% initialisation of solution for SOR
rng(0,'twister');
x0 = 30.*rand(length(b), 1) + 300;

% optimal w
ws = 0.1:0.05:1.9;
iterations = zeros(length(ws), 1);
i = 1;
for w=ws
    [iters, residuals, x] = SOR(w, A, b, x0);
    iterations(i) = iters;
    i = i + 1;
end
plot(ws, iterations);
title("Plot to find optimal w for tol = 0.01");
xlabel("w");
ylabel("Number of iterations");
disp("w_opt = ");
[min_iter, pos] = min(iterations);
ws(pos)
disp("Number of iterations at w_opt = ")
min_iter

%% part 2

%permutation vector
perm = rb_permvec(n, m);
A_rb = A(perm, perm);
figure(1);
spy(A_rb);
b_rb = b(perm);

% initialisation of solution for SOR
rng(0,'twister');
y0 = 30.*rand(length(b), 1) + 300;

% optimal cw
ws = 0.1:0.05:1.99;
iterations = zeros(length(ws), 1);
i = 1;
for w=ws
    [iters, residuals, y] = SOR(w, A_rb, b_rb, y0);
    x(perm) = y;
    iterations(i) = iters;
    i = i + 1;
end
figure(2);
plot(ws, iterations);
title("Plot to find optimal w for tol = 0.01");
xlabel("w");
ylabel("Number of iterations");
disp("w_opt = ");
[min_iter, pos] = min(iterations);
ws(pos)
disp("Number of iterations at w_opt = ")
min_iter

%% part 3

ws = 0.1:0.05:1.99;
rhos = zeros(length(ws), 1);
nrms = zeros(length(ws), 1);
i = 1;
for w=ws
    [rho, nrm] = spectral(w, A);
    rhos(i) = rho;
    nrms(i) = nrm;
    i = i + 1;
end
figure(2);
plot(ws, rhos);
title("Plot to find optimal w");
xlabel("w");
ylabel("rho(G)");
disp("w_opt = ");
[min_rho, pos] = min(rhos);
ws(pos)
disp("rho(G) at w_opt = ")
min_rho

figure(3);
plot(ws, nrms);
title("Plot to find optimal w");
xlabel("w");
ylabel("norm(G)");
disp("w_opt = ");
[min_nrm, pos] = min(nrms);
ws(pos)
disp("rho(G) at w_opt = ")
min_nrm

%% part 4

D = diag(diag(A));
L = -tril(A, -1);
U = -triu(A, 1);
G_sor = (D - w*L)\(w*U + (1.0-w)*D);
G_Jac = D\(L + U);
beta = max(abs(eig(full(G_Jac))));
w_opt_rho = 2.0/(1.0 + sqrt(1.0-beta^2));

ws = 0.1:0.005:1.99;
rhos = zeros(length(ws), 1);
i=1;
for w=ws
    [rho] = part4(w, w_opt_rho, beta);
    rhos(i) = rho;
    i = i+1;
end
figure(1);
plot(ws, rhos);
title("Plot of spectral radius vs w according to Young");
xlabel("w");
ylabel("rho(G)");


% M = (D - w*L);
% N = (w*U + (1.0-w)*D);
% Mp = transpose(M)/transpose(N);
% Np = M\N;
% Ap = Mp - Np;
% Gp_sor = Mp\Np;
% Gp_Jac = diag(diag(Ap))\(-tril(Ap, -1) - triu(Ap, 1));
% beta_p = max(abs(eig(full(Gp_Jac))));
% w_opt_norm = 2.0/(1.0 + sqrt(1.0-beta_p^2));
% 
% ws = 1.2;
% nrms = zeros(length(ws), 1);
% i=1;
% for w=ws
%     [sqnrm] = part4(w, w_opt_norm, beta_p);
%     nrms(i) = sqrt(sqnrm);
%     i = i+1;
% end
% figure(2);
% plot(ws, nrms);
% title("Plot of norm of G vs w according to Young");
% xlabel("w");
% ylabel("norm(G)");
% w_opt_norm, w_opt_rho